import { AppstoreComponent } from './appstore/appstore.component';
import { PluginupComponent } from './pluginup/pluginup.component';
import { TableComponent } from './table/table.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { Table2Component } from './table2/table2.component';
import { EdittableComponent } from './edittable/edittable.component';
import { Edittable2Component } from './edittable2/edittable2.component';

export const routes: Routes = [
  { path: '', component: MainComponent, pathMatch: 'full'},
  { path: 'table1', component: TableComponent },
  { path: 'table2', component: Table2Component},
  { path: 'edittable1', component: EdittableComponent},
  { path: 'edittable2', component: Edittable2Component},
  { path: 'pluginup' , component: PluginupComponent},
  { path: 'appstore', component: AppstoreComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [ MainComponent, TableComponent, Table2Component,
  EdittableComponent, Edittable2Component, MainComponent, PluginupComponent, AppstoreComponent];
