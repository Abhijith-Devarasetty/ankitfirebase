import { Router } from '@angular/router';
import { element } from 'protractor';
import { ItemService } from './../item.service';
import { Component } from '@angular/core';

export interface UsersData {
  name: string;
  id: number;
  color: string;
}


@Component({
  selector: 'app-edittable',
  templateUrl: './edittable.component.html',
  styleUrls: ['./edittable.component.css']
})
export class EdittableComponent {

  action = this.service.action;
  localData = this.service.element;
  table = this.service.tablename;

  constructor(public service: ItemService, public router: Router) {
  }

  doAction() {
      this.service.task(this.action);
      this.router.navigateByUrl(this.service.tablename);
    // this.dialogRef.close({event: this.action, data: this.localData});
  }

  closeDialog() {
    console.log('close');
    this.router.navigateByUrl(this.service.tablename);
  }
}
