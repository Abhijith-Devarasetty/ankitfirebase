import { MatTable } from '@angular/material';
import { Injectable, ViewChild } from '@angular/core';

export interface UsersData {
  name: string;
  id: number;
  color: string;
  contact: number;
  place: string;
  address: string;
  pin: number;
  country: string;
}

const ELEMENT_DATA: UsersData[] = [
  {
    id: 1560608769631,
    name: 'Artificial Intelligence',
    color: 'Red',
    contact: 9898989898,
    place: 'Hyderabad',
    address: '22 TSM Colony',
    pin: 154542,
    country: 'India',
  },
  {
    id: 1560608796011,
    name: 'Machine Learning',
    color: 'Pink',
    contact: 9898777788,
    place: 'New Delhi',
    address: 'New Chandni Chowk',
    pin: 163696,
    country: 'India',
  },
  {
    id: 1560608787811,
    name: 'Robotic Process Automation',
    color: 'Black',
    contact: 9812345685,
    place: 'Chandigarh',
    address: 'Sec 7 Market',
    pin: 160106,
    country: 'India',
  },
  {
    id: 1560608805101,
    name: 'Blockchain',
    color: 'Blue',
    contact: 8989898989,
    place: 'Chennai',
    address: 'Near Airport',
    pin: 256565,
    country: 'India',
  },
];

@Injectable({
  providedIn: 'root',
})
export class ItemService {
  element: any;
  action: string;
  dataSource = ELEMENT_DATA;
  tablename: string;

  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor() {}

  task(action) {
    if (action === 'Delete') {
      console.log('delete1');
      this.dataSource = this.dataSource.filter((value, key) => {
        return value.id !== this.element.id;
      });
    } else if (action === 'Update') {
      console.log('update1');
      this.dataSource = this.dataSource.filter((value, key) => {
        if (value.id === this.element.id) {
          value.name = this.element.name;
          value.color = this.element.color;
        }
        return true;
      });
    } else if (action === 'Add') {
      const d = new Date();
      this.dataSource.push({
        id: d.getTime(),
        name: this.element.name,
        color: this.element.color,
        contact: this.element.contact,
        place: this.element.place,
        address: this.element.address,
        pin: this.element.pin,
        country: this.element.country,
      });
    }
  }
  addRowData(rowObj) {
    const d = new Date();
    this.dataSource.push({
      id: d.getTime(),
      name: rowObj.name,
      color: rowObj.color,
      contact: rowObj.contact,
      place: rowObj.place,
      address: rowObj.address,
      pin: rowObj.pin,
      country: rowObj.country,
    });
    return 'added data';
  }
}
