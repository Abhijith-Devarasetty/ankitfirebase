import { ItemService } from './../item.service';
import { Router } from '@angular/router';
import {
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material';
import { MatTable } from '@angular/material';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'name',
    'color',
    'contact',
    'place',
    'address',
    'pin',
    'country',
    'edit',
    'delete',
  ];
  dataSource = new MatTableDataSource(this.service.dataSource);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(
    public router: Router,
    public service: ItemService
  ) {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    // if (this.dataSource.paginator) {
    //   this.dataSource.paginator.firstPage();
    // }
  }

  openDialog(action, obj, table) {
    this.service.action = action;
    this.service.element = obj;
    this.service.tablename = table;
    this.router.navigateByUrl('/edittable1');
  }

  addRowData(rowObj) {
    this.service.addRowData(rowObj);
    return 'added data';
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
