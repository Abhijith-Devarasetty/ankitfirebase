import { Item2Service } from './item2.service';
import { ItemService } from './item.service';
import { FormsModule } from '@angular/forms';
import { Location } from '@angular/common';
import { TestBed, async, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Router, RouterLinkWithHref, ActivatedRoute, convertToParamMap } from '@angular/router';
import { AppComponent } from './app.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import {routes} from '../app/app-routing.module';
import { Observable, of, Subject } from 'rxjs';
import { TableComponent } from './table/table.component';
import { MainComponent } from './main/main.component';
import { Table2Component } from './table2/table2.component';
import { EdittableComponent } from './edittable/edittable.component';
import { Edittable2Component } from './edittable2/edittable2.component';
import { MatTableModule } from '@angular/material/table';
import { componentFactoryName } from '@angular/compiler';

describe('AppComponent, ', () => {
  let router: Router;
  let location: Location;
  let item: ItemService;
  let item2: Item2Service;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        FormsModule,
        MatTableModule
      ],
      declarations: [
        AppComponent,
        TableComponent,
        MainComponent,
        Table2Component,
        Edittable2Component,
        EdittableComponent
      ],
      providers: [
        Item2Service,
        ItemService
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    }).compileComponents();
  }));

  beforeEach(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.ngZone.run(() => {});
    router = TestBed.get(Router);
    location = TestBed.get(Location);
    item = TestBed.get(ItemService);
    item2 = TestBed.get(Item2Service);
    router.initialNavigation();
  });

  it('should redirect to initial navigation route', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(location.path()).toBe('/');
    });
  }));

  it('should redirect if condition true', fakeAsync(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.debugElement.componentInstance;
    router.navigate(['/table2']);
    fixture.ngZone.run(() => {
    });
    tick();
    expect(location.path()).toBe('/table2');
  }));

  it('should spy `addRowData` function service', () => {
    spyOn(item, 'addRowData');
    const fixture = TestBed.createComponent(TableComponent);
    const component = fixture.componentInstance;
    let sal = component.addRowData('');
    expect(sal).toBe('added data');
    expect(item.addRowData).toHaveBeenCalled();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'UI'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('UI');
  });

  it('should create the menu button', () => {
    const color = 'rgb(255,255,255)';
    const fixture = TestBed.createComponent(AppComponent);
    const btn = fixture.debugElement.query(By.css('mat-icon')).nativeElement;
    expect(btn.innerHTML).toBeTruthy();
    // expect(btn.style.color).toBe(color);
    expect(btn.innerHTML).toEqual('menu');
  });

  it('should have content in app-name ', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const board = fixture.debugElement.query(By.css('.example-app-name')).nativeElement;
    expect(board.innerHTML).not.toBeNull();
    expect(board.innerHTML).toEqual('Virtusa App');
  });

  it('should have sidenav"', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const snav = fixture.debugElement.query(By.css('mat-sidenav')).nativeElement;
    expect(snav.innerHTML).toBeTruthy();
  });

  it('should set sidenav tab to Dashboard', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const atag = fixture.debugElement.query(By.css('.link')).nativeElement;
    expect(atag.innerHTML).toEqual('Dashboard');
  });

  it('should have toolbar background blue', () => {
     const fixture = TestBed.createComponent(AppComponent);
     const app = fixture.debugElement.query(By.css('.example-toolbar')).nativeElement;
     expect(app.style.backgroundColor).toBe('');
   });



  // it('should render title', () => {
  //   const fixture = TestBed.createComponent(AppComponent);
  //   fixture.detectChanges();
  //   const compiled = fixture.debugElement.nativeElement;
  //   expect(compiled.querySelector('.content span').textContent).toContain('UI app is running!');
  // });
});
