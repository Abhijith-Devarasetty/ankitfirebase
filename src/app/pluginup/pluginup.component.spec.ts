import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PluginupComponent } from './pluginup.component';

describe('PluginupComponent', () => {
  let component: PluginupComponent;
  let fixture: ComponentFixture<PluginupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PluginupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PluginupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
