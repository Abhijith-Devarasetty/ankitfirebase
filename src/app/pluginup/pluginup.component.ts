import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-pluginup',
  templateUrl: './pluginup.component.html',
  styleUrls: ['./pluginup.component.css']
})
export class PluginupComponent implements OnInit {

  constructor(public db: AngularFireDatabase) {
    // this.items = db.list('items').valueChanges();
  }
  // title = 'Angular 8 Firebase';
  // description = 'Angular-Fire-Demo';

  // itemValue = '';
  // items: Observable<any[]>;

  // file: File;
  // fileValue;

  // tslint:disable-next-line: member-ordering
  isHovering: boolean;

  files: File[] = [];

  // onSubmit() {
  //   this.db.list('items').push({ content: this.itemValue});
  //   this.itemValue = '';
  // }
  ngOnInit() {
  }

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  onDrop(files: FileList) {
    for (let i = 0; i < files.length; i++) {
      this.files.push(files.item(i));
    }
  }

}
