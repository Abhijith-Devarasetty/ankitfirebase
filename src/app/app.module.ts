import { AngularFirestore } from '@angular/fire/firestore';
import { DropzoneDirective } from './dropzone.directive';
import { MatIconModule } from '@angular/material/icon';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatToolbarModule,
  MatTabsModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
} from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import {
  OverlayModule,
  CdkOverlayOrigin,
  CdkConnectedOverlay,
} from '@angular/cdk/overlay';
import { MainComponent } from './main/main.component';
import { TableComponent } from './table/table.component';
import {
  MatPaginatorModule,
  MatDialogModule,
  MatSidenavModule,
} from '@angular/material';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import { Table2Component } from './table2/table2.component';
import { EdittableComponent } from './edittable/edittable.component';
import { Edittable2Component } from './edittable2/edittable2.component';
import {SidebarModule} from 'primeng/sidebar';
import {ButtonModule} from 'primeng/button';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { environment } from '../environments/environment';
import { PluginupComponent } from './pluginup/pluginup.component';
import { AppstoreComponent } from './appstore/appstore.component';
import { UploadTaskComponent } from './upload-task/upload-task.component';
import { AngularFireStorage } from '@angular/fire/storage';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    TableComponent,
    Table2Component,
    EdittableComponent,
    Edittable2Component,
    PluginupComponent,
    AppstoreComponent,
    UploadTaskComponent,
    DropzoneDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatTabsModule,
    MatCardModule,
    MatSnackBarModule,
    MatMenuModule,
    OverlayModule,
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    SidebarModule,
    ButtonModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  exports: [MatPaginatorModule],
  entryComponents: [],
  providers: [AngularFirestore, AngularFireStorage],
  bootstrap: [AppComponent],
})
export class AppModule {}
