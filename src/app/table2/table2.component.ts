import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Item2Service } from '../item2.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material';

@Component({
  selector: 'app-table2',
  templateUrl: './table2.component.html',
  styleUrls: ['./table2.component.css'],
})
export class Table2Component implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  displayedColumns: string[] = [
    'id',
    'name',
    'color',
    'games',
    'hobbies',
    'class',
    'age',
    'contact',
    'place',
    'address',
    'pin',
    'country',
    'edit',
    'delete',
  ];
  dataSource = new MatTableDataSource(this.service2.dataSource2);


  constructor(public router: Router, public service2: Item2Service) {}

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialog(action, obj, table) {
    this.service2.action = action;
    this.service2.element = obj;
    this.service2.tablename = table;
    this.router.navigateByUrl('/edittable2');
  }
  addRowData(rowObj) {
    this.service2.addRowData2(rowObj);
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
