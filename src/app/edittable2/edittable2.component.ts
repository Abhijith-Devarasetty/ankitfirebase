import { Router } from '@angular/router';
import { element } from 'protractor';
import { Item2Service } from './../item2.service';
import { Component } from '@angular/core';

export interface UsersData {
  name: string;
  id: number;
  color: string;
}


@Component({
  selector: 'app-edittable',
  templateUrl: './edittable2.component.html',
  styleUrls: ['./edittable2.component.css']
})
export class Edittable2Component {

  action = this.service2.action;
  localData = this.service2.element;
  table = this.service2.tablename;

  constructor(public service2: Item2Service, public router: Router) {
  }

  doAction() {
    this.service2.task2(this.action);
    this.router.navigateByUrl(this.service2.tablename);
    // this.dialogRef.close({event: this.action, data: this.localData});
  }

  closeDialog() {
    console.log('close');
    this.router.navigateByUrl(this.service2.tablename);
  }
}
