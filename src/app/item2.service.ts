import { MatTable } from '@angular/material';
import { Injectable, ViewChild } from '@angular/core';

export interface PostsData {
  name: string;
  id: number;
  color: string;
  games: string;
  hobbies: string;
  class: string;
  age: number;
  contact: number;
  place: string;
  address: string;
  pin: number;
  country: string;
}

const POST_DATA: PostsData[] = [
  {id: 1560608769632, name: 'Artificial Intelligence', color: 'Red', games: 'Volleyball', hobbies: 'Guitar', class: '10th', age: 16,
  contact: 9898989898, place: 'Hyderabad', address: '22 TSM Colony', pin: 154542, country: 'India'},
  {id: 1560608796012, name: 'Machine Learning', color: 'Pink', games: 'Volleyball', hobbies: 'Guitar', class: '10th', age: 16,
  contact: 9898989892, place: 'Hyderabad', address: '22 TSM Colony', pin: 154542, country: 'India'},
  {id: 1560608787812, name: 'Robotic Process Automation', color: 'Black' , games: 'Volleyball', hobbies: 'Guitar', class: '10th', age: 16,
  contact: 9898989898, place: 'Hyderabad', address: '22 TSM Colony', pin: 154542, country: 'India'},
  {id: 1560608805102, name: 'Blockchain', color: 'Blue' , games: 'Volleyball', hobbies: 'Guitar', class: '10th', age: 16,
  contact: 9898989898, place: 'Hyderabad', address: '22 TSM Colony', pin: 154542, country: 'India'}
];

@Injectable({
  providedIn: 'root'
})
export class Item2Service {
  element: any;
  action: string;
  dataSource2 =  (POST_DATA);
  tablename: string;
  @ViewChild(MatTable, {static: true}) table: MatTable<any>;

  constructor() { }

  task2(action) {
    if (action === 'Delete') {
      console.log('delete2');
      this.dataSource2 = this.dataSource2.filter((value, key) => {
        return value.id !== this.element.id;
        });
    } else if (action === 'Update') {
      console.log('update2');
      this.dataSource2 = this.dataSource2.filter((value, key) => {
           if (value.id === this.element.id) {
             value.name = this.element.name;
             value.color = this.element.color;
           }
           return true;
        });
    } else if (action === 'Add') {
      const d = new Date();
      this.dataSource2.push({
       id: d.getTime(),
       name: this.element.name,
       color: this.element.color,
       games: this.element.games,
       hobbies: this.element.hobbies,
       class: this.element.class,
       age: this.element.age,
       contact: this.element.contact,
       place: this.element.place,
       address: this.element.address,
       pin: this.element.pin,
       country: this.element.country
     });
    }
  }
    addRowData2(rowObj) {
      const d = new Date();
      this.dataSource2.push({
        id: d.getTime(),
        name: rowObj.name,
        color: rowObj.color,
        games: rowObj.games,
        hobbies: rowObj.hobbies,
        class: rowObj.class,
        age: rowObj.age,
        contact: rowObj.contact,
        place: rowObj.place,
        address: rowObj.address,
        pin: rowObj.pin,
        country: rowObj.country
      });
     }
}
