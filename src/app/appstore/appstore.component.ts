import { AngularFirestore } from '@angular/fire/firestore';
import { UrlTree } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireStorage, createStorageRef } from '@angular/fire/storage';

@Component({
  selector: 'app-appstore',
  templateUrl: './appstore.component.html',
  styleUrls: ['./appstore.component.css']
})
export class AppstoreComponent implements OnInit {
  itemValue = '';
  items: Observable<any[]>;

  file;
  fileValue: Observable<any[]>;

  constructor(public db: AngularFireDatabase, public storage: AngularFireStorage, public store: AngularFirestore) {
    this.items = store.collection('files').valueChanges();
    console.log(this.items);
    // const ref = this.storage.ref('test/1605675202117_20201025144544284.jpg').getDownloadURL;
    // console.log(ref);
    console.log(store.collection('files').get());
  }

  ngOnInit() {
  }

}
