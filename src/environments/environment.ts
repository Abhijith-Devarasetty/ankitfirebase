// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyCBD_lW-DrttEUSn5Gd9MCPX0_lWBkktHE',
    authDomain: 'angular-firebase-app-02.firebaseapp.com',
    databaseURL: 'https://angular-firebase-app-02.firebaseio.com',
    projectId: 'angular-firebase-app-02',
    storageBucket: 'angular-firebase-app-02.appspot.com',
    messagingSenderId: '1021709869283',
    appId: '1:1021709869283:web:38b45d1fb145085c92a5d6',
    measurementId: 'G-1CVBG045B1'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
